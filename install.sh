#!/bin/sh

set -e
set -u

if [ -f `pwd`/shared_utils ]; then
    source `pwd`/shared_utils
else
    echo "Could not find utils functions. Abort..."
    exit 1
fi

system_check() {
    title "Archlinux installation: System checks"
    if ! run_quiet "test -d /sys/firmware/efi/efivars" "checking EFI mode"; then
        error_msg "You haven't booted in EFI mode..."
        error_msg "Please configure your firmware and restart the installation."
        exit 1
    fi

    if ! run_critical "ping -c 2 www.google.fr" "Checking internet connection"; then
        echo "No internet connection detected..."
        if [ ! systemctl is-active dhcpcd ]; then
            echo "dhcpcd disabled. Trying to start it..."
            systemctl start dhcpcd
            sleep 5
            if ! eval "ping -c 2 www.google.fr"; then
                error_msg "Could not establish connection. Aborting script."
                exit 1
            fi
        else
            error_msg "Could not establish connection. Aborting script."
            exit 1
        fi
    fi

    pacman_install reflector
    run_quiet 'reflector --latest 15 --sort rate --save
    /etc/pacman.d/mirrorlist' 'sorting mirrors... '

    run_quiet 'timedatectl set-ntp true' 'setting up ntp...'

    info_msg "Pre-installation checks passed"
    prompt_msg "How do you want to name your system ?"
    read HOSTNAME
    pause_func
}

disk_partition() {
    title "Disk partitionning"
    lsblk

    prompt_msg "Which one is your main drive ?"
    read drive
    run_critical "fdisk ${drive} < part_layout" "Partitionning disk..."
    CRYP_PART=${drive}1
    BOOT_PART=${drive}2

    run_critical "mkfs.vfat -F32 ${BOOT_PART}" "Formatting boot partition..."
}

crypto_setup() {
    run_critical "modprobe dm_crypt" "Loading dm_crypt module..."
    run_critical "cryptsetup --cipher xts-plain64 --key-size 512 --hash sha512" "Setting up encryption..."
    run_critical "cryptsetup luksOpen ${CRYP_PART} ${HOSTNAME}"

    echo
    info_msg "Setting up lvm:"

    run_critical "pvcreate /dev/mapper/${HOSTNAME}" "Initializing physical volume..."
    run_critical "vgcreate ${HOSTNAME} /dev/mapper/${HOSTNAME}" "Create lvm
    volume..."
}

# system_check
# disk_partition
