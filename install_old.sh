#!/bin/sh

sudo pacman --noconfirm --needed --color -Syu ttf-dejavu redshift mu isync
sudo pacman --noconfirm --needed --color -S git rxvt-unicode openssh emacs vim firefox
sudo pacman --noconfirm --needed --color -S chromium i3 xorg-server xorg-xinit
sudo pacman --noconfirm --needed --color -S adobe-source-code-pro-fonts base-devel
sudo pacman --noconfirm --needed --color -S clang cmake

if [ ! -e /etc/makepkg.conf ]; then
    cp makepkg.conf /etc/
fi

cd /tmp
git clone https://aur.archlinux.org/yaourt.git
cd yaourt
makepkg -si

yaourt --noconfirm slack-desktop dropbox unclutter-xfixes-git
