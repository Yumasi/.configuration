import System.Exit
import Data.Maybe (Maybe, isNothing, fromJust)
import qualified Data.List as L
import qualified Data.Map as M
import GHC.IO.Handle

-- Xmonad Core
import XMonad
import qualified XMonad.StackSet as W
import XMonad.Config.Desktop

-- Layouts
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Gaps
import XMonad.Layout.Spacing
import XMonad.Layout.MultiToggle
import XMonad.Layout.NoBorders
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableTile
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.SimpleFloat
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.Minimize

-- Actions
import XMonad.Actions.Navigation2D
import XMonad.Actions.GridSelect
import XMonad.Actions.UpdatePointer
import XMonad.Actions.SpawnOn
import XMonad.Actions.CycleWS

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks

-- Utils
import XMonad.Util.NamedScratchpad
import XMonad.Util.WorkspaceCompare
import XMonad.Util.Run
import XMonad.Util.EZConfig


-- Xmonad entry point
main = do
  xmproc <- spawnPipe "/usr/bin/xmobar"
  xmonad $ myConfig xmproc

-- Config
myConfig xmproc = withNavigation2DConfig def {
  defaultTiledNavigation = centerNavigation
  } $ def {
  -- simple stuff
  terminal = myTerminal,
  focusFollowsMouse = myFocusFollowsMouse,
  clickJustFocuses = myClickJustFocuses,
  borderWidth = myBorderWidth,
  modMask = myModMask,
  workspaces = myWorkspaces,
  normalBorderColor = myNormalBorderColor,
  focusedBorderColor = myFocusedBorderColor,

  -- hooks, layouts
  layoutHook = myLayout,
  manageHook = myManageHook <+> manageHook desktopConfig,
  handleEventHook = myEventHook <+> handleEventHook desktopConfig,
  logHook = (myLogHook xmproc) <+> logHook desktopConfig,
  startupHook = myStartupHook
  } `additionalKeysP` addKeys

-- Log Hook Definition: Custom Xmobar Output + Update Pointer Hook
myLogHook :: Handle -> X ()
myLogHook xmproc = dynamicLogWithPP xmobarPP
                   { ppOutput = hPutStrLn xmproc
                   , ppCurrent = xmobarColor "#51afef" "" . wrap "[" "]"
                   , ppTitle = xmobarColor "#c678dd" "" . shorten 50
                   , ppSort = fmap (.namedScratchpadFilterOutWorkspace) getSortByTag
                   , ppLayout = xmobarColor "#ECBE7B" "" . myIcons
                   } >> updatePointer (0.75, 0.75) (0.75, 0.75)

                   -- Use urxvtc as terminal
myTerminal = "urxvtc"

-- Window focus follows mouse
myFocusFollowsMouse = True

-- Clicking on window focuses and passes the click forward
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth = 4

-- Defines with mod key to use (mod4mask == super)
myModMask = mod4Mask

-- Define and name each workspace
myWorkspaces = ["1: TERM","2: WEB", "3: CODE","4: MAIL","5: TODOS","6: MEDIA", "7: ETC"] ++ map show [8..8] ++ ["NSP"]

-- Border colors
myNormalBorderColor = "#2C323C"
myFocusedBorderColor = "#51afef"


-- Additional keybindings for media keys
addKeys = [
  ("<XF86AudioLowerVolume>"	,spawn "pulseaudio-ctl down 5")
  ]

-- Layout definitions & modifiers
myLayout = onWorkspace "6: MEDIA" simpleFloat $
           desktopLayoutModifiers $
           smartBorders $
           minimize $
           mkToggle (NOBORDERS ?? FULL ?? EOT) layouts
  where
    layouts = gaps [(U,5), (R,5), (L,5), (D,5)] $ spacing 5 (tiled ||| Mirror tiled ||| emptyBSP ||| Full)
    -- tiled resizable tall layout
    tiled = ResizableTall nmaster delta ratio []
    -- The default number of windows in the master pane
    nmaster = 1
    -- Default proportion of screen occupied by master pane
    ratio = 1/2
    -- Percent of screen to increment by when resizing panes
    delta = 3/100

-- Defined icons for various layout types
myIcons layout
  | is "Mirror ResizableTall"  = "<icon=~/.xmonad/icons/layout-mirror.xbm/>"
  | otherwise = "<icon=/home/treize/.xmonad/icons/layout-gimp.xbm/>"
  where is = (`L.isInfixOf` layout)

-- Manage hook defining various window rules
myManageHook = composeAll
  [ className =? "Spotify" --> doCenterFloat
  , resource =? "desktop_window" --> doIgnore
  , isDialog --> doCenterFloat ]
  <+> (isFullscreen --> doFullFloat)
  <+> manageSpawn

-- EventHook
myEventHook = mempty

-- Startup hook, not doing anything
myStartupHook = do
  setWMName "Whatever"
  startupHook desktopConfig
