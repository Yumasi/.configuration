#!/bin/bash
case "$1" in
    powersave)
        sudo pstate-frequency -S -p powersave && xbacklight -set 15
        ;;
    balanced)
        sudo pstate-frequency -S -p balanced && xbacklight -set 50
        ;;
    performance)
        sudo pstate-frequency -S -p performance && xbacklight -set 100
        ;;
    *)
        echo "Usage: $0 {powersave|balanced|performance}"
        exit 2
esac

exit 0
