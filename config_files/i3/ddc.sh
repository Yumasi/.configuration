#!/bin/sh

case "$1" in
	night)
		ddcutil setvcp 10 75 12 75 --bus 4
		ddcutil setvcp 10 75 12 75 --bus 5
		;;
	day)
	    ddcutil setvcp 10 100 12 100 --bus 4
	    ddcutil setvcp 10 100 12 100 --bus 5
	    ;;
	*)
	    echo "Usage: $0 {night|day}"
	    exit 2
esac

exit 0
